<?php 
    session_start();
    set_time_limit(0);
    date_default_timezone_set('Asia/Jerusalem');

    if($_SESSION['userType'] != 0) header('Location: login.php');
    include 'classes/autoloader.inc.php';

    if(isset($_POST['submit'])) {
        $userName = $_POST['userName'];
        $message = $_POST['msgBody'];
        $date = $_POST['timing-date'];
        $hour = $_POST['timing-hour'];
        if(isset($_POST['sendToAll'])) $sendTo = 'All';
        if(isset($_POST['sendToAnswered'])) $sendTo = 'Answered';

        $Sms = new Sms();
        $Sms->sendSms($userName, $message, $date, $hour, $sendTo);
    }
?>

<!DOCTYPE html>
<html lang="heb">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
    <!--<meta name="description" content="...">-->
    <!--<meta name="keywords" content="...">-->
    <meta name="author" content="נתנאל וקנין">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="shortcut icon" type="image/png" href="img/favicon.png"> -->
    <title></title>
</head>
<body>
    <a href="dashboard.php" class="mainPage">
        <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem; transform: rotate(180deg);">
            <use xlink:href="assets/icons/sprite.svg#icon-arrow-left"></use>
        </svg>
        חזרה לתפריט הראשי
    </a>
    <div class="sendSms-wrapper">
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="sendSms-form" method="post" autocomplete="off">
            <label for="userName">שם משתמש של החתן והכלה
                <input type="text" name="userName" id="userName" required>
            </label>
            <label for="msgBody">גוף ההודעה
                <textarea name="msgBody" id="msgBody" cols="30" rows="10" required></textarea>
            </label>
            <label for="timing-date">תאריך ההודעה
                <input type="date" name="timing-date" id="timing-date">
            </label>
            <label for="timing-hour">שעת ההודעה
                <input type="time" name="timing-hour" id="timing-hour">
            </label>
            <div class="smsType-wrapper">
                <label for="sendToAll">
                    <input type="checkbox" id="sendToAll" name="sendToAll" onclick="check(this)">
                    שליחה לכל האורחים
                </label>
                <label for="sendToAnswered">
                    <input type="checkbox" id="sendToAnswered" name="sendToAnswered" onclick="check(this)">
                    שליחה לאורחים שענו 
                </label>
            </div>
            <input type="submit" name="submit" value="שלח\י סמס\ים">
        </form>
    </div>
    <script src="scripts/checkboxes.js"></script>
</body>
</html>