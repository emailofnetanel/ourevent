<?php
session_start();
include 'classes/autoloader.inc.php';
?>
<!DOCTYPE html>
<html lang="heb">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!--<meta name="description" content="...">-->
    <!--<meta name="keywords" content="...">-->
    <meta name="author" content="נתנאל וקנין">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="shortcut icon" type="image/png" href="img/favicon.png"> -->
    <title></title>
</head>

<body>
    <!-- Conditionally render admin menu or wedding responses according to user type -->
    <?php if ($_SESSION['userType'] == 0) { ?>
        <!-- Admin dashboard -->
        <div class="dashboard-wrapper">
            <nav class="menu">
                <ul class="menu-list">
                    <li class="menu-item">
                        <a href="new-user.php">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-user-plus"></use>
                            </svg>
                            הוספת משתמשים
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="upload-guests.php">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-upload2"></use>
                            </svg>
                            העלאת קובץ אורחים
                        </a>
                    </li>
                    <li class="menu-item"><a href="send-sms.php">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-mobile"></use>
                            </svg>
                            שליחת סמסים
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="#">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-users"></use>
                            </svg>
                            ניהול לקוחות ומשימות (לא פעיל)
                        </a>
                    </li>
                    <li class="menu-item"><a href="#">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-user-minus"></use>
                            </svg>
                            הסרת משתמשים (לא פעיל)
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="#">
                            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem;">
                                <use xlink:href="assets/icons/sprite.svg#icon-folder-minus"></use>
                            </svg>
                            מחיקת רשומות (לא פעיל)
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    <?php } else { ?>
        <!-- Customer dashboard -->

    <?php } ?>
</body>

</html>