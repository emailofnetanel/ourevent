<?php 
    class User extends Db {
        private $usersData = [];
        public static $errorArr = [
            "שם המשתמש או הססמא צריכים להכיל לפחות 12 תווים. אנא נסה\י שנית",
            "שם משתמש או ססמא לא נכונים. אנא נסה\י שנית",
            "שם המשתמש שבחרת קיים כבר במערכת. אנא נסה\י שם משתמש אחר.",
            "משתמש חדש נוצר בהצלחה",
            "קובץ האורחים עודכן בהצלחה בבסיס הנתונים."
        ];
        
        public function fetchAllUsers() {
            $sql = "SELECT * FROM users";
            $result = $this->connect()->query($sql);
            $numRows = $result->num_rows;

            if($numRows > 0) {
                while($rows = $result->fetch_assoc()) {
                    $this->usersData[] = $rows['username'];
                    $this->usersData[] = $rows['password'];
                    $this->usersData[] = $rows['userType'];
                }
                return $this->usersData;
            }
        }

        // Login form connection validation and authorization
        public function login($userName, $userPassword) {
            // Checking length and cleaning inputs
            if((strlen($userName) > 3) && (strlen($userName) < 12)) {
                if(strlen($userPassword) > 3 && strlen($userPassword < 12)) {
                    $userName = trim(htmlspecialchars($userName));
                    $userPassword = trim(htmlspecialchars($userPassword));

                    // Search for matching username and password values from db
                    $sql = "SELECT username,password,userType FROM users 
                            WHERE username = '$userName' 
                            AND password = '$userPassword'";
                    $result = $this->connect()->query($sql);
                    $numRows = $result->num_rows;

                    if($numRows > 0) {
                        // Open session and redirect to dashboard page
                        session_start();
                        while($rows = $result->fetch_assoc()) {
                            $_SESSION['userName'] = $userName;
                            $_SESSION['userPassword'] = $userPassword;
                            $_SESSION['userType'] = $rows['userType'];
                            header('Location: dashboard.php');
                        }
                    } else {
                        // User or password incorrect
                        echo self::$errorArr[1];
                    }
                } else echo self::$errorArr[0];
            } else echo self::$errorArr[0];
        }


        // Create new user
        public function newUser($userName, $userPassword, $userType) {
            $userName = trim($userName);
            $userPassword = trim($userPassword);
            if(((strlen($userName) >= 3) && (strlen($userName) <= 12)) && ((strlen($userPassword) >= 3) && (strlen($userPassword) <= 12))) {
                    $userName = trim(htmlspecialchars($userName));
                    $userPassword = trim(htmlspecialchars($userPassword));

                    // Call to fetchAllUsers method for searching if username is already exists
                    $this->usersData = $this->fetchAllUsers();
                    
                    // Check if given username is exists inside userData array
                    if(!in_array($userName, $this->usersData)) {
                        // Insert new user record to users table
                        $sql = "INSERT INTO users(username, password, userType)
                        VALUES('$userName', '$userPassword', '$userType')";
                        if($result = $this->connect()->query($sql)) echo self::$errorArr[3];
                    } else echo self::$errorArr[2];
                } else echo self::$errorArr[0];
        }


        public function uploadGuests($file, $userName) {
            $filename = $file['tmp_name'];            

            // The nested array to hold all the arrays
            $allRows = [];

            // Open the file for reading
            if(($h = fopen("{$filename}", "r")) !== FALSE) {
                // Each line in the file is converted into an individual array that we call $data
                // The items of the array are comma separated
                while (($data = fgetcsv($h, 1000, ",")) !== FALSE){
                    // Each individual array is being pushed into the nested array
                    $allRows[] = $data;
                }
                // Close the file
                fclose($h);
            }

            // Loop over $allRows array and insert data to guests table
            foreach($allRows as $row) {
                $name = $row[0];
                $phone = $row[1];
                $sql = "INSERT INTO guests(name, phone, wedding)
                VALUES('$name', '$phone', '$userName')";
                
                $result = $this->connect()->query($sql);
            }
            if($result) echo self::$errorArr[4];
        }
    }
?>