<?php 
    session_start();
    if (!empty($_SESSION['userName'])) header('Location: dashboard.php');
    include 'classes/autoloader.inc.php';

    if(isset($_POST['submit'])) {
        $user = new User();
        $userName = $_POST['userName'];
        $userPassword = $_POST['userPassword'];
        $user->login($userName,$userPassword);
    }
?>
<!DOCTYPE html>
<html lang="he">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
    <meta name="description" content="Ourevent מערכת סידורי הושבה ואישורי הגעה המובילה בישראל">
    <meta name="keywords" content="סידורי הושבה, אישורי הגעה, סמסים לאירועים, חתונות">
    <meta name="author" content="נתנאל וקנין">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="shortcut icon" type="image/png" href="img/favicon.png"> -->
    <title>התחברות למערכת - Ourevent</title>
</head>

<body>
    <div class="login-wrapper">
        <form 
        action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" 
        class="login-form" 
        method="POST" 
        autocomplete="off"> 
            <label for="userName">שם משתמש:
                <input type="text" name="userName" id="userName" required>
            </label>
            <label for="userPassword">ססמא:
                <input type="password" name="userPassword" id="userPassword" required>
            </label>
            <input type="submit" name="submit" value="התחבר">
        </form>
    </div>
    <script src="scripts/script.js"></script>
</body>

</html>