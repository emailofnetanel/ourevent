<?php
    session_start();
    if($_SESSION['userType'] != 0) header('Location: login.php');
    include 'classes/autoloader.inc.php';

    if(isset($_POST['submit'])) {
        $file = $_FILES['file'];
        $userName = $_POST['userName'];

        $newUpload = new User();
        $newUpload->uploadGuests($file, $userName);
    }
?>

<!DOCTYPE html>
<html lang="he">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
    <!--<meta name="description" content="...">-->
    <!--<meta name="keywords" content="...">-->
    <meta name="author" content="נתנאל וקנין">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="shortcut icon" type="image/png" href="img/favicon.png"> -->
    <title></title>
</head>
<body>
    <div class="uploadGuests-wrapper">
        <a href="dashboard.php" class="mainPage">
            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem; transform: rotate(180deg);">
                <use xlink:href="assets/icons/sprite.svg#icon-arrow-left"></use>
            </svg>
            חזרה לתפריט הראשי
        </a>
        <form 
        action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" 
        class="uploadGuests-form" method="POST" 
        autocomplete="off" 
        enctype="multipart/form-data">
            <div class="inst">
                <ul class="inst-list">
                    <li class="inst-item">1. הקובץ שיעלה למערכת חייב להיות קובץ מסוג CSV בלבד!</li>
                    <li class="inst-item">2. בשל שיקולים של ביצועי המערכת,
                         במידה ויש בקובץ מעל 300 אורחים יש לפצל את האורחים למספר קבצים ולשלוח להם סמסים בנפרד לכל קובץ.</li>
                </ul>
            </div>
            <label for="userName">שם משתמש של החתן והכלה
                <input type="text" id="userName" name="userName">
            </label>
            <label for="file">הוסף קובץ
                <input type="file" id="file" name="file">
            </label>
            <input type="submit" name="submit" value="העלאת רשומות">
        </form>
    </div>
</body>
</html>