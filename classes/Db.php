<?php 
    class Db {
        private $servername = 'localhost';
        private $username = 'root';
        private $password = '';
        private $dbname = 'oureventsms';

        protected function connect() {
            if($conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname)) {
                mysqli_set_charset($conn, "utf8");
                return $conn;
            }
        }
    }
?>