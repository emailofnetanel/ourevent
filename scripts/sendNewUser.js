// Handle new-user.php form
var submitBtn = document.querySelector('.submit');
var userName = document.querySelector('#userName');
var userPassword = document.querySelector('#userPassword');
var customer = document.querySelector('#customer');
var employee = document.querySelector('#employee');
var userType;

submitBtn.addEventListener('click', function(e){
    e.preventDefault();

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://localhost/ourevent/new-user.php', true);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    if(customer.checked) {
        userType = 'customer';
    } else if(employee.checked){
        userType = 'employee';
    }

    // Prepare data to send
    var data = `userName=${userName.value}&userPassword=${userPassword.value}&userType=${userType}&submit=submit`;

    xhr.onload = function() {
        if(this.status == 200) {
            alert(this.responseText);
        }
    }

    // Error handler
    xhr.onerror = function() {
      console.log('Request Error');
    }
    
    // Fire request
    xhr.send(data);
});
