<?php 
    class Sms extends Db{
        private static $url = 'https://www.019sms.co.il:8090/api';

        public function sendSms($userName, $message, $date = '', $time = '', $to ) {
            // Format to NOW time if there is no timing set from the employee
            if ($date == '') {
                $date = date('d/m/y');
            } else {
                // Change the date value to 2 digits year instead 4 (HTML date input default) digits format
                $defaultFormat = 'd/m/Y';
                $date = DateTime::createFromFormat($defaultFormat, $date);
                $date->format('d/m/y');
            }

            // Determine the sms timing.
            // 019 Will not accept empty "timing" xml tag.
            // For that reason +10 minutes will be added from current time if no time received by the employee
            if ($time == '') {
                $time = date("H:i");
                $currentTime = strtotime($time);
                $futureTime = $currentTime+(60*10);
                $time = date("H:i", $futureTime);
            }

            // Determine the query according to $to
            $to == 'All' ? $sql = "SELECT * FROM guests WHERE wedding = '$userName'" 
            : $sql = "SELECT * FROM guests WHERE wedding = '$userName' AND response > 0";

            $result = $this->connect()->query($sql);
            $numRows = $result->num_rows;

            if($numRows > 0) {
                // Loop trough guests phone numbers
                while($rows = $result->fetch_assoc()) {
                    $phone = $rows['phone'];
                    // Remove dashes from phone numbers
                    $phone = str_replace('-', '', $phone);
                    $phoneNumbers[] = $phone;
                }

                // Loop trough all phone numbers for sending them sms
                for($i = 0; $i < count($phoneNumbers); $i++) {
                    //Prepare sms
                    $xml = '<?xml version="1.0" encoding="UTF-8"?>' . 
                    "<sms>
                    <user>
                    <username>netanel1</username>
                    <password>netanel123</password>
                    </user>
                    <source>019</source>
                    <destinations>
                    <phone>$phoneNumbers[$i]</phone>
                    </destinations>
                    <message>$message</message>
                    <timing>". $date . ' ' . $time ."</timing>
                    <response>1</response>
                    </sms>";

                    // Fire sms
                    $CR = curl_init();
                    curl_setopt($CR, CURLOPT_URL, self::$url);
                    curl_setopt($CR, CURLOPT_POST, 1);
                    curl_setopt($CR, CURLOPT_FAILONERROR, true); 
                    curl_setopt($CR,CURLOPT_POSTFIELDS, $xml); 
                    curl_setopt($CR,CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($CR,CURLOPT_HTTPHEADER,array("charset=utf-8"));
                    
                    $result = curl_exec($CR);
                    $error = curl_error ($CR);

                    if(!empty( $error )) die("Error: ".$error);
                    else $response = new SimpleXMLElement($result);
                }
                return true;
            } else {
                echo 'לא נמצאו תוצאות מתאימות לבקשתך. נסה\י לבחור שם משתמש אחר.';
            }
        }
    }