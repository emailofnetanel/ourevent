<?php 
    spl_autoload_register(function($className) {
        $ext = '.php';
        require_once __DIR__ . '\\' . $className . $ext;
    });
?>