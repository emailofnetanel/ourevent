<?php 
    session_start();
    if($_SESSION['userType'] != 0) header('Location: login.php');
    include 'classes/autoloader.inc.php';
    if(isset($_POST['submit'])) {
        $user = new User();
        $userName = $_POST['userName'];
        $userPassword = $_POST['userPassword'];
        $userType = $_POST['userType'];
        return $user->newUser($userName, $userPassword, $userType);
    }
    

?>

<!DOCTYPE html>
<html lang="he">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Heebo&display=swap" rel="stylesheet">
    <!--<meta name="description" content="...">-->
    <!--<meta name="keywords" content="...">-->
    <meta name="author" content="נתנאל וקנין">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="shortcut icon" type="image/png" href="img/favicon.png"> -->
    <title></title>
</head>
<body>
    <div class="newUser-wrapper">
        <a href="dashboard.php" class="mainPage">
            <svg style="fill:#fff; font-weight: bold; width: 2rem; height: 2rem; transform: rotate(180deg);">
                <use xlink:href="assets/icons/sprite.svg#icon-arrow-left"></use>
            </svg>
            חזרה לתפריט הראשי
        </a>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" class="newUser-form" method="POST" autocomplete="off">
            <label for="userName">שם משתמש
                <input type="text" name="userName" id="userName" required>
            </label>
            <label for="userPassword">ססמא
                <input type="password" name="userPassword" id="userPassword" required>
            </label>
            <div class="role-wrapper">
                <label for="customer">
                    <input type="checkbox" name="customer" id="customer" onclick="check(this)">
                    לקוח
                </label>
                <label for="employee">
                    <input type="checkbox" name="employee" id="employee" onclick="check(this)">
                    עובד Ourevent
                </label>
            </div>
            <input type="submit" name="submit" value="יצירת משתמש חדש" class="submit">
        </form>
    </div>
    <script src="scripts/sendNewUser.js"></script>
    <script src="scripts/checkboxes.js"></script>
</body>
</html>